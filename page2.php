<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP MCQ Form</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>

<body>
    <?php
    session_start();

    $answersQ6 = array("network", "port", "expose", "history");
    $answersQ7 = array("none", "host", "custom", "bridge");
    $answersQ8 = array("ps", "ls", "images", "top");
    $answersQ9 = array("Oracle", "Amazon", "Apple", "Google");
    $answersQ10 = array("Docker Swarm", "Kubernetes", "Mesos", "All");

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST["answer6"]) && isset($_POST["answer7"]) && isset($_POST["answer8"]) && isset($_POST["answer9"]) && isset($_POST["answer10"])) {
            $answer6 = $_POST["answer6"];
            $answer7 = $_POST["answer7"];
            $answer8 = $_POST["answer8"];
            $answer9 = $_POST["answer9"];
            $answer10 = $_POST["answer10"];
            $qa6 = "Q6: Để kiểm tra các port được expose của một container, sử dụng câu lệnh <tt>docker ___ [container-id]</tt>.";
            $qa7 = "Q7: Docker sử dụng default network nào?";
            $qa8 = "Q8: Để xem các tiến trình đang chạy trong một container, sử dụng câu lệnh <tt>docker ___ [container-id]</tt>.";
            $qa9 = "Q9: K8S được phát triển bởi ___.";
            $qa10 = "Q10: Đâu là các công nghệ điều phối containers?";
            setcookie("qa6", $qa6, time() + (86400 * 30), "/");
            setcookie("qa7", $qa7, time() + (86400 * 30), "/");
            setcookie("qa8", $qa8, time() + (86400 * 30), "/");
            setcookie("qa9", $qa9, time() + (86400 * 30), "/");
            setcookie("qa10", $qa10, time() + (86400 * 30), "/");
            setcookie("answer6", $answer6, time() + (86400 * 30), "/");
            setcookie("answer7", $answer7, time() + (86400 * 30), "/");
            setcookie("answer8", $answer8, time() + (86400 * 30), "/");
            setcookie("answer9", $answer9, time() + (86400 * 30), "/");
            setcookie("answer10", $answer10, time() + (86400 * 30), "/");
            header("location: score.php");
        } else {
            echo "<div style='color: red;font-size: 22px;font-weight: bold;
                                text-align: center;'>Hãy trả lời tất cả các câu hỏi</div>";
        }
    }
    ?>
    <div class="container">
        <h1>Docker Multiple Choice Questions</h1>
        <form name="qaForm" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <h3>Q6: Để kiểm tra các port được expose của một container, sử dụng câu lệnh <tt>docker ___ [container-id]</tt>.</h3>
                <ol>
                    <?php
                    foreach ($answersQ6 as $i) {
                        echo "<li> <input class='tick-question' type='radio' value=" . $i . " name='answer6'>" . $i . "</li>";
                    }
                    ?>
                </ol>
            </div>
            <br />
            <div class="form-group">
                <h3>Q7: Docker sử dụng default network nào?</h3>
                <ol>
                    <?php
                    foreach ($answersQ7 as $i) {
                        echo "<li> <input class='tick-question' type='radio' value=" . $i . " name='answer7'>" . $i . "</li>";
                    }
                    ?>
                </ol>
            </div>
            <br />
            <div class="form-group">
                <h3>Q8: Để xem các tiến trình đang chạy trong một container, sử dụng câu lệnh <tt>docker ___ [container-id]</tt>.</h3>
                <ol>
                    <?php
                    foreach ($answersQ8 as $i) {
                        echo "<li> <input class='tick-question' type='radio' value=" . $i . " name='answer8'>" . $i . "</li>";
                    }
                    ?>
                </ol>
            </div>
            <div class="form-group">
                <h3>Q9: K8S được phát triển bởi ___.</h3>
                <ol>
                    <?php
                    foreach ($answersQ9 as $i) {
                        echo "<li> <input class='tick-question' type='radio' value=" . $i . " name='answer9'>" . $i . "</li>";
                    }
                    ?>
                </ol>
            </div>
            <div class="form-group">
                <h3>Q10: Đâu là các công nghệ điều phối containers?</h3>
                <ol>
                    <?php
                    foreach ($answersQ10 as $i) {
                        echo "<li> <input class='tick-question' type='radio' value=" . $i . " name='answer10'>" . $i . "</li>";
                    }
                    ?>
                </ol>
            </div>

            <div class="form-group">
                <input type="submit" value="Tiếp" name="next" class="btn btn-primary" />
            </div>
        </form>
    </div>
</body>

</html>