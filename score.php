<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Score</title>
    <style>
       .message{
            color: blue;
            text-align: center;
       }
    </style>
</head>
<body>
    <?php
        $trueAnswers = array("Container", "run", "images", "Go", "stop", "port", "bridge", "top", "Google", "All");
        $answersList = array();
        $qa_list = array();
        for($i = 1 ; $i<=10 ; $i++){
            $qa = "qa".strval($i);
            array_push($qa_list,$_COOKIE[$qa]);
            $key = "answer".strval($i);
            array_push($answersList, $_COOKIE[$key]);
        }
        $mark = 0;
        for ($i=0; $i < count($answersList); $i++) {
            if($trueAnswers[$i]==$answersList[$i]){
                $mark++;
            }
        }
        echo "<h2 class='message'>Bạn đạt được : $mark/10 </h2>";
        if($mark<4){
            echo "<div class='message'>Bạn quá kém, cần ôn tập thêm</div>";
        }elseif($mark<=7){
            echo "<div class='message'>Cũng bình thường</div>";
        }else{
            echo "<div class='message'>Sắp sửa làm được trợ giảng lớp PHP</div>";
        }
        for ($i=0; $i < count($answersList); $i++) {
            echo "<h4>".$qa_list[$i]."</h4>";
            if($trueAnswers[$i]==$answersList[$i]){
                echo "<input type='radio' checked='checked'>
                <div style='color: green;display: inline;'>".$trueAnswers[$i]."</div>";
            }else{
                echo "<input type='radio' checked='checked'>
                <div style='color: red;display: inline;'>".$answersList[$i]."</div>";
                echo "<br>";
                echo "<div style='color: green;display: inline;padding-left: 25px'>".$trueAnswers[$i]."</div>";
            }
            echo "<br>";
        }
    ?>
</body>
</html>