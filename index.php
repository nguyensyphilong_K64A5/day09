<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP MCQ Form</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>

<body>
    <?php
    session_start();

    $answersQ1 = array("Hub", "Container", "Cloud", "File");
    $answersQ2 = array("create", "run", "build", "sudo");
    $answersQ3 = array("view", "ls", "images", "logs");
    $answersQ4 = array("Java", "C", "C++", "Go");
    $answersQ5 = array("stop", "remove", "halt", "kill");

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST["answer1"]) && isset($_POST["answer2"]) && isset($_POST["answer3"]) && isset($_POST["answer4"]) && isset($_POST["answer5"])) {
            $answer1 = $_POST["answer1"];
            $answer2 = $_POST["answer2"];
            $answer3 = $_POST["answer3"];
            $answer4 = $_POST["answer4"];
            $answer5 = $_POST["answer5"];
            $qa1 = "Q1: ___ là các instances của docker images mà có thể thực thi được bằng câu lệnh <tt>docker run</tt>.";
            $qa2 = "Q2: Để chạy một container trong Docker, sử dụng câu lệnh <tt>docker ___</tt>.";
            $qa3 = "Q3: Để xem danh sách các Docker images, sử dụng câu lệnh <tt>docker ___</tt>.";
            $qa4 = "Q4: Docker được viết bằng ngôn ngữ gì?";
            $qa5 = "Q5: Để dừng một container đang chạy, sử dụng câu lệnh <tt>______</tt>.";
            setcookie("qa1", $qa1, time() + (86400 * 30), "/");
            setcookie("qa2", $qa2, time() + (86400 * 30), "/");
            setcookie("qa3", $qa3, time() + (86400 * 30), "/");
            setcookie("qa4", $qa4, time() + (86400 * 30), "/");
            setcookie("qa5", $qa5, time() + (86400 * 30), "/");
            setcookie("answer1", $answer1, time() + (86400 * 30), "/");
            setcookie("answer2", $answer2, time() + (86400 * 30), "/");
            setcookie("answer3", $answer3, time() + (86400 * 30), "/");
            setcookie("answer4", $answer4, time() + (86400 * 30), "/");
            setcookie("answer5", $answer5, time() + (86400 * 30), "/");
            header("location: page2.php");
        } else {
            echo "<div style='color: red;font-size: 22px;font-weight: bold;
                                text-align: center;'>Hãy trả lời tất cả các câu hỏi</div>";
        }
    }
    ?>
    <div class="container">
        <h1>Docker Multiple Choice Questions</h1>
        <form name="qaForm" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <h3>Q1: ___ là các instances của docker images mà có thể thực thi được bằng câu lệnh <tt>docker run</tt>.</h3>
                <ol>
                    <?php
                    foreach ($answersQ1 as $i) {
                        echo "<li> <input class='tick-question' type='radio' value=" . $i . " name='answer1'>" . $i . "</li>";
                    }
                    ?>
                </ol>
            </div>
            <br />
            <div class="form-group">
                <h3>Q2: Để chạy một container trong Docker, sử dụng câu lệnh <tt>docker ___</tt>.</h3>
                <ol>
                    <?php
                    foreach ($answersQ2 as $i) {
                        echo "<li> <input class='tick-question' type='radio' value=" . $i . " name='answer2'>" . $i . "</li>";
                    }
                    ?>
                </ol>
            </div>
            <br />
            <div class="form-group">
                <h3>Q3: Để xem danh sách các Docker images, sử dụng câu lệnh <tt>docker ___</tt>.</h3>
                <ol>
                    <?php
                    foreach ($answersQ3 as $i) {
                        echo "<li> <input class='tick-question' type='radio' value=" . $i . " name='answer3'>" . $i . "</li>";
                    }
                    ?>
                </ol>
            </div>
            <div class="form-group">
                <h3>Q4: Docker được viết bằng ngôn ngữ gì?</h3>
                <ol>
                    <?php
                    foreach ($answersQ4 as $i) {
                        echo "<li> <input class='tick-question' type='radio' value=" . $i . " name='answer4'>" . $i . "</li>";
                    }
                    ?>
                </ol>
            </div>
            <div class="form-group">
                <h3>Q5: Để dừng một container đang chạy, sử dụng câu lệnh <tt>______</tt>.</h3>
                <ol>
                    <?php
                    foreach ($answersQ5 as $i) {
                        echo "<li> <input class='tick-question' type='radio' value=" . $i . " name='answer5'>" . $i . "</li>";
                    }
                    ?>
                </ol>
            </div>

            <div class="form-group">
                <input type="submit" value="Tiếp" name="next" class="btn btn-primary" />
            </div>
        </form>
    </div>
</body>

</html>